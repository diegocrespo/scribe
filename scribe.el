(defun scribe-extract-zip (zip-file)
  "Takes a path to a zip file and then runs the appropriate shell command to unzip it and returns the path to the unzipped file"
  (let* ((zip-name (file-name-nondirectory zip-file)) ; get the file name without the directory
         (folder-name (file-name-sans-extension zip-name)) ; get the file name without the extension
         (output-dir (file-name-directory zip-file)) ; get the full path of the output directory
         (output-path (expand-file-name  folder-name output-dir)) ; path to save the unzipped file
	 )
    (unless (file-directory-p output-path)
      (make-directory output-path t))
    
    (message "The zipname is %s" zip-name)
    (message "The folder-name is %s" folder-name)
    (message "The output-dir is %s" output-dir)
    (message "The output-path is %s" output-path)    
    ;; (message "The output-path is %s" output-path)    
    (message "Running the command tar -xf %s -C %s" zip-file output-path)
    (shell-command (format "tar -xf %s -C %s" zip-file output-path))
    output-path))

(defun scribe-string-contains? (substring string)
  "Check if the STRING contains the SUBSTRING."
  (string-match-p (regexp-quote substring) string))

(defun scribe-read-xml-file (filepath)
  "Read the contents of an XML file and return as a string."
  (with-temp-buffer
    (insert-file-contents filepath)
    (libxml-parse-xml-region (point-min) (point-max))))

(defun scribe-multiselect (a lat)
  "Return a list of all occurrences of A from nested list LAT."
  (cond
   ((null lat) nil)
   ((stringp lat)  '())
   ((eq (car lat) a)
    (cons lat (scribe-multiselect a (cdr lat))))
   ((listp (car lat)) 
    (append 
     (scribe-multiselect a (car lat)) 
     (scribe-multiselect a (cdr lat))))
   (t
    (scribe-multiselect a (cdr lat)))))

(defun scribe-extract-string (list)
  "get the string out of the paragraph"
  (cond
   ((null list) nil)
   ((stringp (car list)) (car list))
   (t(scribe-extract-string (cdr list)))))

(defun scribe-extract-text (list)
  "collect all texts from paragraphs"
  (cond
   ;; can't use nil because nil can appear in list 
   ((null list) '(end-list))
   ;; remove the nil in the list
   (t (cons (scribe-extract-string (car list)) (scribe-extract-text (cdr list))))))

(defun scribe-concatenate-strings (lst)
  "Concatenates strings in a list, separating them with new lines.
   Stops when encountering the symbol 'end-list."
  (let ((result ""))
    (dolist (item lst result)
      (cond
        ((stringp item)
         (setq result (concat result item "\n")))
        ((null item)
         (setq result (concat result "\n")))
        ((eq item 'end-list)
         (return result))))))

(defun scribe-concat-strings (lst)
  "Concatenates strings in a list, separating them with new lines.
   Stops when encountering the symbol 'end-list."
  (let ((result "")
        (continue t))
    (dolist (item lst result)
      (when continue
        (cond
          ((stringp item)
           (setq result (concat result item "\n")))
          ((null item)
           (setq result (concat result "\n")))
          ((eq item 'end-list)
           (setq continue nil)))))))

(defun append-text-to-buffer (text)
  (with-current-buffer (get-buffer-create "temp-buffer")
    (goto-char (point-max))
    (insert (concat text "\n"))))


(scribe-extract-zip "test-files/simple_paragraphs.odt")
(setq files (directory-files "test-files/simple_paragraphs/" t))
(setq filtered-list (cl-remove-if-not (lambda (item) (scribe-string-contains? "content.xml" item)) files))
(setq xml-content (scribe-read-xml-file (car filtered-list)))

(setq bod (scribe-multiselect 'body xml-content))
(setq p (scribe-multiselect 'p xml-content))

(setq texts (scribe-extract-text p))

(setq result (scribe-concat-strings texts) )

(with-current-buffer (get-buffer-create "temp-buffer")
  (insert result))

